<?php

namespace App\Services;

use App\Models\Transaction;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TransactionService
{
    public function getOne($id)
    {
        try{
            $result = Transaction::findOrFail($id);

            return $result;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function getAll($request)
    {
        try{
            $results = Transaction::when($request->text_search, function($query) use ($request){
                $query->where('title', 'like', '%'. $request->text_search .'%');
            })
            ->orderBy('id', 'desc')
            ->paginate($request->perpage ?: 10);

            return $results;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function store($request)
    {
        try{
            DB::beginTransaction();

            $result = Transaction::create([
                'title' => $request->title,
                'amount' => $request->amount,
                'type' => $request->type
            ]);

            DB::commit();

            return $result;

        }catch(\Throwable $th){
            DB::rollback();
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    //ditanyalan adit Transaksi
    public function update($request, Transaction $transaction)
    {
        try{
            DB::beginTransaction();

            $result = $transaction->update([
                'title' => $request->title,
                'amount' => $request->amount,
                'type' => $request->type
            ]);

            DB::commit();

            return $result;

        }catch(\Throwable $th){
            DB::rollback();
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function delete(Transaction $transaction)
    {
        try {
            DB::beginTransaction();

            $result = $transaction->delete();

            DB::commit();

            return $result;

        } catch (\Throwable $th) {
            DB::rollback();
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function getOneTrash($id)
    {
        try{
            $result = Transaction::onlyTrashed()->find($id);

            return $result;
        }catch(\Throwable $th){
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function getTrashed($request)
    {
        try {
            $results = Transaction::onlyTrashed()->when($request->text_search, function ($query) use ($request){
                $query->where('title', 'like', '%'. $request->text_search .'%');
            })
            ->orderBy('id', 'desc')
            ->paginate($request->perpage ?: 10);

            return $results;
        } catch (\Throwable $th) {
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function restoreTrash(Transaction $transaction)
    {
        try {
            DB::beginTransaction();

            $result = $transaction->restore();

            DB::commit();

            return $result;
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }

    public function deleteTrash(Transaction $transaction)
    {
        try {
            DB::beginTransaction();

            $result = $transaction->forceDelete();

            DB::commit();

            return $result;
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error('Service error', $th->getMessage());
            abort(500);
        }
    }
}