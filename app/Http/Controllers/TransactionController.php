<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Services\TransactionService;
use App\Http\Requests\Transaction\StoreRequest;
use App\Http\Requests\Transaction\UpdateRequest;
use App\Models\Transaction;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, TransactionService $service)
    {
        try{
            $result = $service->getAll($request);

            return $result;
        }catch(\Throwable $th){
            Log::error('Controller error '. $th->getMessage());
            abort(500);
        }
    }

    public function store(StoreRequest $request, TransactionService $service)
    {
        try{
            $result = $service->store($request);
            
            return $result;
            
        }catch(\Throwable $th){
            Log::error('Controller error '. $th->getMessage());
            abort(500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, TransactionService $service, $id)
    {
        try {
            $result = $service->update($request, $service->getOne($id));
            
            return $result;
        } catch (\Throwable $th) {
            Log::error('Controller error '. $th->getMessage());
            abort(500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(TransactionService $service, $id)
    {
        try {
            $result = $service->delete($service->getOne($id));

            return $result;

        } catch (\Throwable $th) {
            Log::error('Controller error '. $th->getMessage());
            abort(500);
        }
    }

    public function getTrashed(Request $request, TransactionService $service)
    {
        try {
            $results = $service->getTrashed($request);

            return $results;

        } catch (\Throwable $th) {
            Log::error('Controller error '. $th->getMessage());
            abort(500);
        }
    }

    public function restoreTrash(TransactionService $service, $id)
    {
        try {
            $result = $service->restoreTrash($service->getOneTrash($id));

            return $result;

        } catch (\Throwable $th) {
            Log::error('Controller error '. $th->getMessage());
            abort(500);
        }
    }

    public function deleteTrash(TransactionService $service, $id)
    {
        try {
            $result = $service->deleteTrash($service->getOneTrash($id));

            return $result;

        } catch (\Throwable $th) {
            Log::error('Controller error '. $th->getMessage());
            abort(500);
        }
    }
}
