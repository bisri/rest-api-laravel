<?php

use App\Http\Controllers\Auth\LoginContoller;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Auth\RegisterContoller;
use App\Http\Controllers\TransactionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
|--------------------------------------------------------------------------
| ROUTE GAWAN
|--------------------------------------------------------------------------
| Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
|    return $request->user();
| });
|
*/

/*
|--------------------------------------------------------------------------
| AUTH
*/
Route::group(['prefix'=> 'auth'], function(){
    Route::post('/login', LoginContoller::class);
    Route::post('/logout', LogoutController::class);
    Route::post('/register', RegisterContoller::class);
});

/*
|--------------------------------------------------------------------------
| MANAGE TRANSACTION
*/
Route::group(['prefix' => 'transaction', 'middleware' => ['jwt.verify']], function () {
    Route::get('/', [TransactionController::class, 'index']);
    Route::post('/create', [TransactionController::class, 'store']);
    Route::put('/update/{id}', [TransactionController::class, 'update']);
    Route::delete('/delete/{id}', [TransactionController::class, 'destroy']);
    Route::get('/trash', [TransactionController::class, 'getTrashed']);
    Route::delete('/trash/delete/{id}', [TransactionController::class, 'deleteTrash']);
    Route::get('/trash/restore/{id}', [TransactionController::class, 'restoreTrash']);
});
